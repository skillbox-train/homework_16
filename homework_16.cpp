#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <ctime>

int main()
{
    const int N = 5; 
    int A[N][N];

    time_t t;
    time(&t);
    int k = (localtime(&t)->tm_mday) % N;

    int DZ = k & N; 

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            A[i][j] = (i + j);
            std::cout << A[i][j];

        }
        std::cout << "\n";
    }

    int sum = 0;
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            if (i == DZ) sum += A[i][j];
        }
    }
    std::cout << "Sum of Elements " << " = " << sum;
    return 0;
}
